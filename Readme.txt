## SENG3400 Assignment 2 ##
Local Time Converter

An introduction to Java Web Services using WDSL and Axis.

This program runs a local server to which clients can connect and get the time in other (simulated) time zones. Admins may change the local database of time zones.

Server:
1. Manually compile MyLTCServer.java, MyLTCAdmin.java, and LTCdb.java using javac
2. Copy both source files and compiled .class files to tomcat\webapps\axis\WEB-INF\jwsClasses
3. Copy MyLTCServer.java and MyLTCAdmin.java to tomcat\webapps\axis
4. Rename to .jws file types
5. Copy LTCdb.class to tomcat\webapps\axis\WEB-INF\classes

Clients:
1. Generate WSDL files from browser (or use mine if the settings are default), save to individual folders
2. Copy log4j.properties from axis\lib to both folders
3. Generate Java classes using WSDL2Java
4. Compile the generated classes
5. Compile MyUserAdmin.java and MyUserClient.java
6. Run clients with appropriate arguments
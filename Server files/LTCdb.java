/*
 * SENG 3400- Assignment 2
 * Author:		Christian Lassen
 * Student num: 3121707
*/

import java.util.HashMap;
import java.lang.StringBuilder;

public final class LTCdb {
	private static int count = 0;
	private static HashMap<String, Double> db;
	static {
		db = new HashMap<String, Double>();
		db.put("SYDNEY", 10.0);
		db.put("AMSTERDAM", 2.0);
		db.put("LOS ANGELES", -7.0);
		db.put("BEIJING", 8.0);
	}

	// Returns whether or not the location exists in the database
	public static boolean hasLocation(String loc) {
		return db.containsKey(loc);
	}
	
	// Returns the offset for the supplied location
	public static double getOffset(String loc) {
		return db.get(loc);
	}
	
	// Returns a list of all the locations in the database
	public static String getAllLocations() {
		StringBuilder sb = new StringBuilder();
		for(String s : db.keySet()) {
			sb.append(s + ", " + db.get(s) + "\n");
		}
		return sb.toString();
	}
	
	// Inserts the location and it's offset in the database
	// Returns true after insertion
	public static boolean add(String loc, double offset) {
		db.put(loc, offset);
		return true;
	}
	
	// Returns the call count for the server
	public static int getCount() {
		return count;
	}
	
	// Increments the count
	public static void incrementCount() {
		count++;
	}
}
/*
 * SENG 3400- Assignment 2
 * Author:		Christian Lassen
 * Student num: 3121707
*/


public class MyLTCAdmin {
	
	// Wow, such security
	static final String adminuser = "admin";
	static final String password = "converter";
	
	public static boolean addLocation(String user, String pwd, String location, double offset) {
		LTCdb.incrementCount();
		if(validate(user, pwd) && !LTCdb.hasLocation(location)) {
			return LTCdb.add(location, offset);
		}
		return false;
	}
	public static boolean setOffset(String user, String pwd, String location, double offset) {
		LTCdb.incrementCount();
		if(validate(user, pwd) && LTCdb.hasLocation(location)) {
			return LTCdb.add(location, offset);
		}
		return false;
	}
	
	public static int callCount(String user, String pwd) {
		if(validate(user, pwd))
			return LTCdb.getCount();
		return -1;
	}
	
	private static boolean validate(String user, String pwd) {
		return (user.equals(adminuser) && pwd.equals(password));
	}
}
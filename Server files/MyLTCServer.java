/*
 * SENG 3400- Assignment 2
 * Author:		Christian Lassen
 * Student num: 3121707
*/


public class MyLTCServer {
	
	// Returns the offset for the supplied location
	// Returns NaN if the supplied location does not exist
	public static double currentOffset(String location) {
		if(LTCdb.hasLocation(location))
			return LTCdb.getOffset(location);
		LTCdb.incrementCount();
			return Double.NaN;
	}

	// Returns a list of all the locations in the database
	public static String listLocations() {
		LTCdb.incrementCount();
		return LTCdb.getAllLocations();
	}
	
	// Takes two locations and one timestamp (HH:MM) 
	// Calculates the timedifference and applies it to the timestamp
	// Returns the timestamp as a string in the same format
	public static String convert(String from, String to, String time) {
		
		// Increment count
		LTCdb.incrementCount();
		
		// Parse input
		String[] times;
		int hours;
		int minutes;
		try {
			times = time.split(":");
			hours = Integer.parseInt(times[0]);
			minutes = Integer.parseInt(times[1]);
		} catch (Exception e) {
			e.printStackTrace();
			return "Invalid time format\nUse HH:MM";
		}
		
		// Check for invalid time input
		if(hours < 0 || hours > 23 || minutes < 0 || minutes > 59)
			return "Invalid time values\nUse HH:MM";
		
		// Check if locations exist
		if(!LTCdb.hasLocation(from) || !LTCdb.hasLocation(to))
			return "Location(s) do not exist in database\nUse listLocations to see available time zones";
			
		
		// Convert total number of hours to minutes
		int totalMinutes = hours*60+minutes;
		double fromOffset = LTCdb.getOffset(from);
		double toOffset = LTCdb.getOffset(to);

		// Calculate time difference in minutes
		int fromTotalMinutes = calcMinutes(fromOffset);
		int toTotalMinutes = calcMinutes(toOffset);
		int totalDifference = toTotalMinutes-fromTotalMinutes;

		// Add time difference to current time (may be negative)
		totalMinutes += totalDifference;

		// Finally calculate converted time
		int newMinutes = (totalMinutes % 60 + 60) % 60;
		int newHours = (totalMinutes / 60 + 24) % 24;

		// Produce result string with padded zeros
		return String.format("%02d:%02d", newHours, newMinutes);
	}
	
	// Takes a time offset in floating point and converts to number of minutes
	public static int calcMinutes(double t) {
		double time = Math.abs(t);
		int floor = (int) Math.floor(time);	// Takes the number of hours
		
		// ((X.YY - X) *100) Creates an integer value of the first two decimal points
		int ct = (int) (Math.round((time-=floor)*100));	
		floor *= 60;						// Number of hours * 60mins per hour
		System.out.println(ct+", "+floor);
		return (t<0) ? (-1*(ct+floor)) : (ct+floor);
	}
}
/*
 * SENG 3400- Assignment 2
 * Author:		Christian Lassen
 * Student num: 3121707
*/


import localhost.axis.MyLTCServer_jws.*;

public class MyUserClient {
	public static void main(String[] args) {
		
		// Simple check for number of parameters. Will break on incorrect usage!
		if(args.length < 1 || args.length > 4) {
			System.out.println("Incorrect number of parameters");
			System.exit(0);
		}
		try {			
			MyLTCServerService ltcs = new MyLTCServerServiceLocator();
			MyLTCServer ltc = ltcs.getMyLTCServer();
			
			// Parse request input
			String request = args[0];
			String response = "";
			double offset = 0.0;
			
			// Switch on request
			switch(request) {
				case "currentOffset":
					offset = ltc.currentOffset(args[1]);
					System.out.println("Current offset: " + offset);
					break;
				case "listLocations":
					response = ltc.listLocations();
					System.out.println("Time zones in database");
					System.out.println(response);
					break;
				case "convert":
					response = ltc.convert(args[1], args[2], args[3]);
					System.out.println(response);
					break;
				default:
					System.out.println("Invalid request");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
/*
 * SENG 3400- Assignment 2
 * Author:		Christian Lassen
 * Student num: 3121707
*/


import localhost.axis.MyLTCAdmin_jws.*;

public class MyUserAdmin {
	public static void main(String[] args) {
		
		// Simple check for number of parameters. Does not check incorrect usage
		if(args.length < 3 || args.length > 5) {
			System.out.println("Incorrect number of parameters");
			System.exit(0);
		}
		try {			
			MyLTCAdminService ltcs = new MyLTCAdminServiceLocator();
			MyLTCAdmin ltc = ltcs.getMyLTCAdmin();
			
			// Parse input
			String request = args[0];
			String username = args[1];
			String password = args[2];
			
			double offset = 0.0;
			String result = "";
			boolean success;
			
			// Switch on request
			switch(request) {
				case "addLocation":
					offset = Double.parseDouble(args[4]);
					success = ltc.addLocation(username, password, args[3], offset);
					result = success ? "Successfully added" : "Error: Already exists, or incorrect log-in";
					System.out.println(result);
					break;
				case "setOffset":
					offset = Double.parseDouble(args[4]);
					success = ltc.setOffset(username, password, args[3], offset);
					result = success ? "Successfully updated" : "Error: Location does not exist in database, or incorrect log-in";
					System.out.println(result);
					break;
				case "callCount":
					int count = ltc.callCount(username, password);
					System.out.println("Call count: " + count);
					break;
				default:
					System.out.println("Invalid request");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}